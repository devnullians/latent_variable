# script to test alsa-libs/test/latency (and related) binary: it compiles it, 
# copies binaries, .so files and configs to phone

# created by: antonior@andrew.cmu.edu

#!/bin/bash

# global script variables (server side, alsa-lib binary names, etc.)
SERVER_NAME=griffin.ini.cmu.edu
SERVER_USERNAME=devnullian
SERVER_SANDBOX=workbench/sandbox/alsa
SERVER_ADB=/home/devnullian/Android/Sdk/platform-tools/adb

LATENT_VARIABLE_REPO_DIR=/home/adamiaonr/Workbench
LATENCY_BIN=latency
LIBASOUND_SO=libasound.so.2.0.0

usage () {
    echo "usage: ./upload-latency --pem-file <path-to-pem-file> [ --create-backup || --revert-configs <backup-timestamp> || --upload-bins <path-to-local-alsa-lib> || --upload-configs <path-to-local-configs> || --compile || --help ]"
    echo "  --pem-file <path-to-pem-file>               - path to SSH .pem file to access server (MANDATORY)"
    echo "  --create-backup                             - creates backup of binaries and configs at server side"
    echo "  --revert-configs <backup-timestamp>         - revert configs on phone, given the timestamp of a previous backup"
    echo "  --upload-bins <path-to-local-alsa-lib>      - upload binaries to phone (latency and .so libs) given the path of the local alsa-lib folder"
    echo "  --upload-configs <path-to-local-configs>    - upload configs to phone, given the path of the local configs folder"
    echo "  --compile                                   - compile the alsa-lib binaries (must be used together with --upload-bins)"
    echo "  --help                                      - prints this menu"
}

# script options
CREATE_BACKUP=0
REVERT_CONFIGS=0
COMPILE_BIN=0
UPLOAD_BIN=0
UPLOAD_CONFIGS=0

if [[ $# -eq 0 ]]; then
    echo "ERROR: no arguments supplied"
    usage
    exit
fi

while [[ "$1" != "" ]]; do
    
    case $1 in

        --create-backup )               CREATE_BACKUP=1
                                        ;;
        --pem-file )                    shift
                                        PEM_FILE=$1
                                        ;;
        --revert-configs )              shift
                                        BACKUP_TIMESTAMP=$1
                                        REVERT_CONFIGS=1
                                        ;;
        --upload-bins )                 shift
                                        SOURCES_DIR=$1
                                        UPLOAD_BIN=1
                                        ;;
        --upload-configs )              shift
                                        CONFIGS_DIR=$1
                                        UPLOAD_CONFIGS=1
                                        ;;
        --compile )                     COMPILE_BIN=1
                                        ;;
        --help )                        usage
                                        exit
                                        ;;
        * )                             usage
                                        exit
        esac
        shift

done

if [[ -z $PEM_FILE ]]; then
    echo "ERROR: must provide .pem file path"
    usage
    exit
fi

if [[ ($COMPILE_BIN -eq 1) && ($UPLOAD_BIN -eq 0) ]]; then
    echo "ERROR: --compile option requires --upload-bins option"
    usage   # print usage
    exit
fi

# 0) create temp folder on server and create backups (if requested)

# 0.1) create temp folder on server for the configs and binaries for this 
# session
TIMESTAMP=$(date +%s)
TEMP_FOLDER=$SERVER_SANDBOX/$TIMESTAMP
ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'mkdir -p /home/$SERVER_USERNAME/$TEMP_FOLDER'"

# 0.2) create backup for current phone configs
if [[ $CREATE_BACKUP -eq 1 ]]; then
    
    BACKUP_FOLDER="/home/$SERVER_USERNAME/$SERVER_SANDBOX/backup/$TIMESTAMP"
    
    ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'mkdir -p $BACKUP_FOLDER; cd $BACKUP_FOLDER; $SERVER_ADB root; $SERVER_ADB pull /data/share/; $SERVER_ADB pull /data/$LATENCY_BIN; $SERVER_ADB pull /data/$LIBASOUND_SO'"
    ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'cd $BACKUP_FOLDER; cd ..; tar -zcvf $TIMESTAMP.tar.gz $TIMESTAMP/; rm -rf $TIMESTAMP'"
fi

# 0.3) revert configs to backup
if [[ $REVERT_CONFIGS -eq 1 ]]; then

    # 0.3.1) extract contents of backup folder
    BACKUP_FOLDER="/home/$SERVER_USERNAME/$SERVER_SANDBOX/backup/$BACKUP_TIMESTAMP"
    ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'mkdir -p $BACKUP_FOLDER; cd $BACKUP_FOLDER; tar -zxvf ../$BACKUP_TIMESTAMP.tar.gz'"

    # 0.3.2) restore binaries
    ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'cd $BACKUP_FOLDER; $SERVER_ADB root; $SERVER_ADB push $LATENCY_BIN /data/; $SERVER_ADB shell 'chmod 765 /data/$LATENCY_BIN'; $SERVER_ADB push $LIBASOUND_SO /data/'; $SERVER_ADB shell 'chmod 765 /data/$LIBASOUND_SO'; $SERVER_ADB shell 'ln -s /data/$LIBASOUND_SO /data/libasound.so.2'"

    # 0.3.3) restore configs
    ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'cd $BACKUP_FOLDER; $SERVER_ADB root; $SERVER_ADB push alsa/ /data/share/alsa/'"

    # 0.3.4) don't go forward if this option is chosen
    exit
fi

# 1) compile ./latency and libs
if [[ $COMPILE_BIN -eq 1 ]]; then

    # 1.1) libasound.so.2.0.0
    cd $SOURCES_DIR

    export PATH=$LATENT_VARIABLE_REPO_DIR/latent_variable/alsa/toolchain/bin/:$PATH
    export CC=arm-linux-androideabi-gcc 
    export CXX=arm-linux-androideabi-g++
    export LD_LIBRARY_PATH=$SOURCES_DIR/src/.libs/

    CC=arm-linux-androideabi-gcc CFLAGS=-fPIC LDFLAGS=-pie ./configure --host=arm-linux --prefix=/data --enable-shared=yes --enable-static=no --disable-python
    make clean
    make

    # 1.2) ./latency
    cd $SOURCES_DIR/test/
    make latency
fi

# 2) upload configs & binaries to temp folder on server

# 2.1) upload binaries 
if [[ $UPLOAD_BIN -eq 1 ]]; then

    # 2.1.1) libasound.so.2.0.0
    cd $SOURCES_DIR/src/.libs
    scp -i $PEM_FILE $LIBASOUND_SO $SERVER_USERNAME@$SERVER_NAME:$TEMP_FOLDER

    # 2.1.2) ./latency
    cd $SOURCES_DIR/test/.libs
    scp -i $PEM_FILE $LATENCY_BIN $SERVER_USERNAME@$SERVER_NAME:$TEMP_FOLDER

    # 2.1.3) copy binaries to /data/, set appropriate permissions and symlinks
    ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'cd /home/$SERVER_USERNAME/$TEMP_FOLDER; $SERVER_ADB root; $SERVER_ADB push $LATENCY_BIN /data/; $SERVER_ADB push $LIBASOUND_SO /data/; $SERVER_ADB shell chmod 765 /data/$LATENCY_BIN; $SERVER_ADB shell chmod 765 /data/$LIBASOUND_SO; $SERVER_ADB shell ln -s /data/$LIBASOUND_SO /data/libasound.so.2'"
fi

# 2.2) upload configs
if [[ $UPLOAD_CONFIGS -eq 1 ]]; then

    cd $CONFIGS_DIR
    scp -i $PEM_FILE -r $CONFIGS_DIR $SERVER_USERNAME@$SERVER_NAME:$TEMP_FOLDER

    # 2.2.1) send configs to appropriate locations on the phone 
    # (/data/share/alsa/)
    ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'cd /home/$SERVER_USERNAME/$TEMP_FOLDER; $SERVER_ADB root; $SERVER_ADB push configs/share/ /data/share/'"
fi

ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'cd /home/$SERVER_USERNAME/$TEMP_FOLDER; cd ..; rm -rf $TIMESTAMP'"

exit
