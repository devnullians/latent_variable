#!/bin/bash

# script adapted from Dominik Seichter's in here:
# http://domseichter.blogspot.com/2008/02/visualize-dependencies-of-binaries-and.html
# adapted by adamiaonr@cmu.edu
 
# This is the maximum depth to which dependencies are resolved
MAXDEPTH=50

ANDROID_SYSTEM_LIB_DIR=/home/devnullian/workbench/latent_variable/cyanogenmod/android/system/out/target/product/peregrine/system/lib

# EDIT: add the dir of the Android toolchain to PATH 
LATENT_VARIABLE_REPO_DIR=/home/devnullian/workbench
export PATH=$LATENT_VARIABLE_REPO_DIR/latent_variable/alsa/toolchain/bin/:$PATH

# EDIT: used arm-linux-androideabi-readelf or arm-linux-androideabi-objdump for
# Android
export READELF_ANDROID=arm-linux-androideabi-readelf
export OBJDUMP_ANDROID=arm-linux-androideabi-objdump
 
# analyze a given file on its
# dependecies using ldd and write
# the results to a given temporary file
#
# Usage: analyze [OUTPUTFILE] [INPUTFILE]
function analyze
{
    local OUT=$1

    # EDIT: check if a .so (if so, assume that the file is in Android's 
    # /system/lib/ dir)
    if [ ${2: -3} == ".so" ]; then
        local IN=$ANDROID_SYSTEM_LIB_DIR/$2
    else
        local IN=$2
    fi

    local NAME=$(basename $IN)
 
    for i in $LIST
    do
        if [ "$i" == "$NAME" ];
        then
            # This file was already parsed
            return
        fi
    done
    # Put the file in the list of all files
    LIST="$LIST $NAME"
 
    DEPTH=$[$DEPTH + 1]
    if [ $DEPTH -ge $MAXDEPTH ];
        then
        echo "MAXDEPTH of $MAXDEPTH reached at file $IN."
        echo "Continuing with next file..."
 # Fix by Marco Nelissen for the case that MAXDEPTH was reached
 DEPTH=$[$DEPTH - 1]
        return
    fi
 
    echo "Parsing file:              $IN"
 
    $READELF $IN &> $READELFTMPFILE
    ELFRET=$?
 
    if [ $ELFRET != 0 ];
        then
        echo "ERROR: ELF reader returned error code $RET"
        echo "ERROR:"
        cat $TMPFILE
        echo "Aborting..."
        rm $TMPFILE
        rm $READELFTMPFILE
        rm $LDDTMPFILE
        exit 1
    fi
 
    DEPENDENCIES=$(cat $READELFTMPFILE | grep NEEDED | awk '{if (substr($NF,1,1) == "[") print substr($NF, 2, length($NF) - 2); else print $NF}')

    for DEP in $DEPENDENCIES;
    do
        if [ -n "$DEP" ]; then
 
            # EDIT: the original script required ldd due to .so files contained 
            # in paths other than /system/lib
#            ldd $IN &> $LDDTMPFILE
            $OBJDUMP_ANDROID -x $IN | grep NEEDED | awk '{print $2}' &> $LDDTMPFILE
            LDDRET=$?
 
            grep "alsa" $LDDTMPFILE

            if [ $LDDRET != 0 ]; then
                echo "ERROR: ldd returned error code $RET"
                echo "ERROR:"
                cat $TMPFILE
                echo "Aborting..."
                rm $TMPFILE
                rm $READELFTMPFILE
                rm $LDDTMPFILE
                exit 1
            fi
 
            DEPPATH=$(grep $DEP $LDDTMPFILE)
            if [ -n "$DEPPATH" ]; then
                echo -e "  \"$NAME\" -> \"$DEP\";" >> $OUT
                analyze $OUT $DEPPATH
            fi
        fi
    done
 
    DEPTH=$[$DEPTH - 1]
}

########################################
# main                                 #
########################################
if [ $# != 2 ]; then
    echo "Usage:"
    echo "  $0 [filename] [output prefix]"
    echo ""
    echo "This tools analyses a shared library or an executable"
    echo "and generates a dependency graph as both .txt and .png files (the "
    echo "prefix for both files should be passed in the 2nd script argument)"
    echo ""
    echo "GraphViz must be installed for this tool to work."
    echo ""
    exit 1
fi

DEPTH=0
INPUT=$1
OUTPUT=$2
TMPFILE=$(mktemp -t)
LDDTMPFILE=$(mktemp -t)
READELFTMPFILE=$(mktemp -t)
LIST=""

# EDIT: check if a .so (if so, assume that the file is in Android's 
# /system/lib/ dir)
if [ ${INPUT: -3} == ".so" ]; then
    INPUT_AUX=$ANDROID_SYSTEM_LIB_DIR/$INPUT
else
    INPUT_AUX=$INPUT
fi

if [ ! -e $INPUT_AUX ]; then
    echo "ERROR: File not found: $INPUT"
    echo "Aborting..."
    exit 2
fi

# Use either readelf or dump
# Linux has readelf, Solaris has dump
# READELF=$(type readelf 2> /dev/null)
# if [ $? != 0 ]; then
#   READELF=$(type dump 2> /dev/null)
#   if [ $? != 0 ]; then
#     echo Unable to find ELF reader
#     exit 1
#   fi
#   READELF="dump -Lv"
# else
#   READELF="readelf -d"
# fi

# EDIT: altered for use with Android readelf and objdump tools
READELF=$(type $READELF_ANDROID 2> /dev/null)
if [ $? != 0 ]; then
    READELF=$(type $OBJDUMP_ANDROID 2> /dev/null)
    
    if [ $? != 0 ]; then
        echo Unable to find ELF reader
        exit 1
    fi

    READELF="$OBJDUMP_ANDROID -Lv"
else
    READELF="$READELF_ANDROID -d"
fi 

echo "Analyzing dependencies of: $INPUT"
echo "Creating output as:        $OUTPUT"
echo ""
 
echo "digraph DependencyTree {" > $TMPFILE
echo "  \"$(basename $INPUT)\" [shape=box];" >> $TMPFILE
analyze $TMPFILE "$INPUT"
echo "}" >> $TMPFILE
#cat $TMPFILE # output generated dotfile for debugging purposses
dot -Tpng $TMPFILE -o$OUTPUT.png
 
rm $LDDTMPFILE
cp $TMPFILE $OUTPUT.txt
rm $TMPFILE
exit 0