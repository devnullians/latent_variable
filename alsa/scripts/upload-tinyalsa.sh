# script to test tinyalsa: it compiles it, 
# copies binaries, .so files and configs to phone

# created by: antonior@andrew.cmu.edu

#!/bin/bash

# global script variables (server side, tinyalsa binary names, etc.)
SERVER_NAME=griffin.ini.cmu.edu
SERVER_USERNAME=devnullian
SERVER_SANDBOX=workbench/sandbox/tinyalsa
SERVER_ADB=/home/devnullian/Android/Sdk/platform-tools/adb

LATENT_VARIABLE_REPO_DIR=/home/adamiaonr/Workbench

TINYPLAY=tinyplay
TINYCAP=tinycap
TINYWAVINFO=tinywavinfo
TINYPCMINFO=tinypcminfo

LIBTINYALSA_SO=libtinyalsa.so

usage () {
    echo "usage: ./upload-tinyalsa --pem-file <path-to-pem-file> [ --create-backup || --revert-configs <backup-timestamp> || --upload-bins <path-to-local-alsa-lib> || --upload-configs <path-to-local-configs> || --compile || --help ]"
    echo "  --pem-file <path-to-pem-file>               - path to SSH .pem file to access server (MANDATORY)"
    echo "  --create-backup                             - creates backup of binaries and configs at server side"
    echo "  --revert-configs <backup-timestamp>         - revert configs on phone, given the timestamp of a previous backup"
    echo "  --upload-bins <path-to-local-tinyalsa-lib>      - upload binaries to phone given the path of the local tinyalsa folder"
    echo "  --upload-configs <path-to-local-configs>    - upload configs to phone, given the path of the local configs folder"
    echo "  --compile                                   - compile the tinyalsa binaries (must be used together with --upload-bins)"
    echo "  --help                                      - prints this menu"
}

# script options
CREATE_BACKUP=0
REVERT_CONFIGS=0
COMPILE_BIN=0
UPLOAD_BIN=0
UPLOAD_CONFIGS=0

if [[ $# -eq 0 ]]; then
    echo "ERROR: no arguments supplied"
    usage
    exit
fi

while [[ "$1" != "" ]]; do
    
    case $1 in

        --create-backup )               CREATE_BACKUP=1
                                        ;;
        --pem-file )                    shift
                                        PEM_FILE=$1
                                        ;;
        --revert-configs )              shift
                                        BACKUP_TIMESTAMP=$1
                                        REVERT_CONFIGS=1
                                        ;;
        --upload-bins )                 shift
                                        SOURCES_DIR=$1
                                        UPLOAD_BIN=1
                                        ;;
        --upload-configs )              shift
                                        CONFIGS_DIR=$1
                                        UPLOAD_CONFIGS=1
                                        ;;
        --compile )                     COMPILE_BIN=1
                                        ;;
        --help )                        usage
                                        exit
                                        ;;
        * )                             usage
                                        exit
        esac
        shift

done

if [[ -z $PEM_FILE ]]; then
    echo "ERROR: must provide .pem file path"
    usage
    exit
fi

if [[ ($COMPILE_BIN -eq 1) && ($UPLOAD_BIN -eq 0) ]]; then
    echo "ERROR: --compile option requires --upload-bins option"
    usage   # print usage
    exit
fi

# 0) create temp folder on server and create backups (if requested)

# 0.1) create temp folder on server for the configs and binaries for this 
# session
TIMESTAMP=$(date +%s)
TEMP_FOLDER=$SERVER_SANDBOX/$TIMESTAMP
ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'mkdir -p /home/$SERVER_USERNAME/$TEMP_FOLDER'"

# 0.2) create backup for current phone configs
if [[ $CREATE_BACKUP -eq 1 ]]; then
    
    BACKUP_FOLDER="/home/$SERVER_USERNAME/$SERVER_SANDBOX/backup/$TIMESTAMP"
    
    ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'mkdir -p $BACKUP_FOLDER; cd $BACKUP_FOLDER; $SERVER_ADB root; $SERVER_ADB pull /data/share/; $SERVER_ADB pull /data/$TINYPLAY; $SERVER_ADB pull /data/$TINYCAP; $SERVER_ADB pull /data/$TINYPCMINFO; $SERVER_ADB pull /data/$TINYWAVINFO; $SERVER_ADB pull /data/$LIBTINYALSA_SO'"
    ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'cd $BACKUP_FOLDER; cd ..; tar -zcvf $TIMESTAMP.tar.gz $TIMESTAMP/; rm -rf $TIMESTAMP'"
fi

# 0.3) revert configs to backup
if [[ $REVERT_CONFIGS -eq 1 ]]; then

    # 0.3.1) extract contents of backup folder
    BACKUP_FOLDER="/home/$SERVER_USERNAME/$SERVER_SANDBOX/backup/$BACKUP_TIMESTAMP"
    ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'mkdir -p $BACKUP_FOLDER; cd $BACKUP_FOLDER; tar -zxvf ../$BACKUP_TIMESTAMP.tar.gz'"

    # 0.3.2) restore binaries
    ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'cd $BACKUP_FOLDER; $SERVER_ADB root; $SERVER_ADB push $TINYPLAY /data/; $SERVER_ADB push $TINYCAP /data/; $SERVER_ADB push $TINYPCMINFO /data/; $SERVER_ADB push $TINYWAVINFO /data/; $SERVER_ADB push $LIBTINYALSA_SO /data/'"

    # 0.3.3) restore configs
    ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'cd $BACKUP_FOLDER; $SERVER_ADB root; $SERVER_ADB push alsa/ /data/share/alsa/'"

    # 0.3.4) don't go forward if this option is chosen
    exit
fi

# 1) compile tinyalsa
if [[ $COMPILE_BIN -eq 1 ]]; then

    export PATH=$LATENT_VARIABLE_REPO_DIR/latent_variable/alsa/toolchain/bin/:$PATH

    cd $SOURCES_DIR

    make clean
    make
fi

# 2) upload configs & binaries to temp folder on server

# 2.1) upload binaries 
if [[ $UPLOAD_BIN -eq 1 ]]; then

    cd $SOURCES_DIR

    scp -i $PEM_FILE $LIBTINYALSA_SO $SERVER_USERNAME@$SERVER_NAME:$TEMP_FOLDER

    scp -i $PEM_FILE $TINYPLAY $SERVER_USERNAME@$SERVER_NAME:$TEMP_FOLDER
    scp -i $PEM_FILE $TINYCAP $SERVER_USERNAME@$SERVER_NAME:$TEMP_FOLDER
    scp -i $PEM_FILE $TINYWAVINFO $SERVER_USERNAME@$SERVER_NAME:$TEMP_FOLDER
    scp -i $PEM_FILE $TINYPCMINFO $SERVER_USERNAME@$SERVER_NAME:$TEMP_FOLDER

    # 2.1.3) copy binaries to /data/, set appropriate permissions and symlinks
    ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'cd /home/$SERVER_USERNAME/$TEMP_FOLDER; $SERVER_ADB root; $SERVER_ADB push $TINYPLAY /data/; $SERVER_ADB push $TINYCAP /data/; $SERVER_ADB push $TINYPCMINFO /data/; $SERVER_ADB push $TINYWAVINFO /data/; $SERVER_ADB push $LIBTINYALSA_SO /data/'"
fi

# 2.2) upload configs
if [[ $UPLOAD_CONFIGS -eq 1 ]]; then

    cd $CONFIGS_DIR
    scp -r $CONFIGS_DIR $SERVER_USERNAME@$SERVER_NAME:$TEMP_FOLDER

    # 2.2.1) send configs to appropriate locations on the phone 
    # (/data/share/alsa/)
    ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'cd /home/$SERVER_USERNAME/$TEMP_FOLDER; $SERVER_ADB root; $SERVER_ADB push configs/share/ /data/share/'"
fi

ssh -i $PEM_FILE $SERVER_USERNAME@$SERVER_NAME bash -c "'cd /home/$SERVER_USERNAME/$TEMP_FOLDER; cd ..; rm -rf $TIMESTAMP'"

exit
