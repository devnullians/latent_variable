# script to test check which pids are using /dev/snd/pcmC0D* devices

# created by: antonior@andrew.cmu.edu

#!/bin/bash

for FILE in /dev/snd/*
do
    # check which PID is using the pcmC0D* device file
    PID="$(fuser $FILE)"

    # if fuser command successful, print the device + <pid, process name>
    if [[ $? -eq 0 ]]; then

        PID="$(echo $PID | tr -d '\n')"

        NAME="$(cat /proc/$PID/cmdline)"
        echo "$FILE : [ $PID,  $NAME ]"
    fi
done

exit
