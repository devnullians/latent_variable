# example taken from "Android NDK Best Practices: Best Practices for authors of 
# libraries", in http://ndkinfo.vleu.net/best-practices-authors

##########################
## libdevnullian-logger ##
##########################

LOCAL_PATH:= $(call my-dir)

# needed by the NDK build system
include $(CLEAR_VARS)

# name your library
LOCAL_MODULE := libdevnullian

# this indicates the path to the headers that users
# of your library wants (like libfoo.h)
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include

# those are the libraries we are using.
# Android.mk will setup linking and includes for you
#LOCAL_SHARED_LIBRARIES := wiz-1.0 bar-2.1
#LOCAL_STATIC_LIBRARIES := wuz-2.3

# this is a path to headers you do not wish to expose.
# please do not put relative paths to the headers of your dependencies here.
# instead, please add the dependencies, to LOCAL_SHARED_LIBRARIES and rely
# on the dependencies to set their LOCAL_EXPORT_C_INCLUDES .
LOCAL_C_INCLUDES := $(LOCAL_PATH)/include

# please do not add GCC optimization or "-g" flags here, as they should
# be set project-wise by the user
# please do not set include paths here (see LOCAL_C_INCLUDES).
# please do not add ARM-specific defines here (but see below).
#LOCAL_CFLAGS := -DENABLE_MAGIC

# if you want to add ARM-specific defines, please do it this way
#ifeq ($(TARGET_ARCH),arm)
#LOCAL_CFLAGS += -DENABLE_ARM_SPECIFIC_MAGIC
#endif

# you can either use this or explicitely list the source code files
#LOCAL_SRC_FILES := $(notdir $(wildcard $(LOCAL_PATH)/*.c))
# if you explicitly list source files, you do not need to repeat $(LOCAL_PATH)
LOCAL_SRC_FILES := logger.c   

# use this only when linking with system libraries that are part
# of the Android ABI.
# for the other libraries, please use LOCAL_SHARED_LIBRARIES
# and LOCAL_STATIC_LIBRARIES as demonstrated above.
#LOCAL_LDLIBS := -lz

# this enables the build of your library as a shared library.
# please do so, if possible, as you would on a GNU/Linux system.
include $(BUILD_SHARED_LIBRARY)
#include $(BUILD_STATIC_LIBRARY)

# this enables the build of a static lib which seems to be necessary for 
# the compilation of some modules. not sure why though...
ifeq ($(HOST_OS), linux)
include $(CLEAR_VARS)
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_SRC_FILES := logger.c
LOCAL_MODULE := libdevnullian
include $(BUILD_HOST_STATIC_LIBRARY)
endif
