#ifndef LOGGER_H
#define LOGGER_H

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <sys/mman.h>
#include <sys/time.h>
#include <limits.h>
#include <syslog.h>

// constants
#define MAX_LOG_ENTRY_SIZE 128
#define MAX_LOG_ENTRY_NUM 8192

// strings
#define DEVNULLIAN_LOGGER "devnullian.log"
#define DEVNULLIAN_LOGGER_TERMCHAR "\n"

void init_log(char ** main_buffer, unsigned int size);
void add_log_entry(
	char ** main_buffer, 	// double pointer: buffer will be altered in function
	char * source_file, 
	char * function_name, 
	char * msg);
void flush_to_syslog(char ** main_buffer);

#endif