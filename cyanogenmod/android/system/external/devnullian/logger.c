/* logger.c
*
* a logger library for the android latency project.
* 
* created by: antonior@andrew.cmu.edu, soojinm@andrew.cmu.edu
* 
*/

#include <devnullian/logger.h>

void init_log(char ** main_buffer, unsigned int size) 
{
    openlog("slog", LOG_PID | LOG_CONS, LOG_USER);
    syslog(LOG_INFO, "[devnullian::logger.c::init_log()] N/A");

    if (strlen((*main_buffer)) > 0) {

        syslog(LOG_ERR, "[devnullian::logger.c::init_log()] ERROR: buffer already initialized");
        closelog();

        return;
    }

    if (size < (MAX_LOG_ENTRY_NUM + 1)) {

        (*main_buffer) = (char *) calloc((MAX_LOG_ENTRY_NUM * MAX_LOG_ENTRY_SIZE), sizeof(char));

    } else {

        syslog(LOG_ERR, "[devnullian::logger.c::init_log()] ERROR: size too large "\
            "(%d, must be < %d)", size, MAX_LOG_ENTRY_NUM);
    }

    closelog();
}

void add_log_entry(char ** main_buffer, char * source_file, char * function_name, char * msg)
{
    // // just allocate memory for a new buffer, if not already done...
    // init_log(main_buffer, MAX_LOG_ENTRY_NUM);

    // char log_entry[MAX_LOG_ENTRY_SIZE];

    struct timeval tv;
    gettimeofday(&tv, NULL); 

    openlog("slog", LOG_PID | LOG_CONS, LOG_USER);
    syslog(LOG_INFO, "%s; %ld.%ld; %s::%s() : %s%s", 
        DEVNULLIAN_LOGGER, tv.tv_sec, tv.tv_usec, source_file, function_name, msg, DEVNULLIAN_LOGGER_TERMCHAR);
    closelog();

    // snprintf(
    //     log_entry, MAX_LOG_ENTRY_SIZE, 
    //     "%s; %ld.%ld; %s::%s() : %s%s", 
    //     DEVNULLIAN_LOGGER, tv.tv_sec, tv.tv_usec, source_file, function_name, msg, DEVNULLIAN_LOGGER_TERMCHAR);

    // // strncat() the log_entry in the main buffer
    // strncat((*main_buffer), log_entry, MAX_LOG_ENTRY_SIZE);

    // // flush to syslog and clear buffer if full
    // if (strlen((*main_buffer)) > (MAX_LOG_ENTRY_SIZE * (MAX_LOG_ENTRY_NUM - 1))) {
    //     flush_to_syslog(main_buffer);
    // }
}

void flush_to_syslog(char ** main_buffer)
{
    openlog("slog", LOG_PID | LOG_CONS, LOG_USER);

    // flush everything in the main buffer to syslog, one \n at a time. we 
    // use strtok() for this.
    char * log_entry = strtok((*main_buffer), DEVNULLIAN_LOGGER_TERMCHAR);

    while (log_entry != NULL) {

        // write to syslog (are we writting the '\n' ?)
        syslog(LOG_INFO, log_entry);

        // capture the new token
        log_entry = strtok(NULL, DEVNULLIAN_LOGGER_TERMCHAR);
    }

    closelog();

    // clear the buffer
    memset((*main_buffer), 0, MAX_LOG_ENTRY_SIZE * MAX_LOG_ENTRY_NUM);
}
