#Audio Latency in AndroidOS
*15-712:2015 Course Project Report*

*Adwait Dongare, Soo-Jin Moon, Antonio Rodrigues*

##Introduction
@everyone @last

##Framework
@soojin
###Audio in Linux
@damiao
###Audio in AndroidOS
@soojin
###Ideal Low-Latency Audio System
@adwait
##Experiments
@adwait

###Methodology
#### Componentwise Measurement
@damiao @adwait
#### End-to-End Measurement
@adwait

###Setup
@adwait
####Loopback Dongle
@adwait

![Schematic of loopback dongle for end-to-end measurement][audio-loopback-dongle]

*Schematic of loopback dongle for end-to-end measurement*

####Timestamps
@adwait @damiao

##Results & Discussion
###End-to-End Latency
@soojin
![Latency for different openSL buffer sizes][buffer-latency]

*Latency for different openSL buffer sizes*

###Componentwise Latency
@damiao
![][ioctl-read]
![][ioctl-write]
![][transfer-read]
![][transfer-write]
###Real-Time Priorities
@adwait
![Comparision of latency with & without real-time priorities on audio threads][realtime-latency]

*Comparision of latency with & without real-time priorities on audio threads*

###Locking
@adwait @damiao

##Related Work
@soojin @damiao

Superpowered, SAPA, changes to core android

##Conclusion & Future Work
@everyone

##References

[audio-loopback-dongle]: images/Audio-Loopback-Dongle.png
[realtime-latency]: images/rt_latency.png
[buffer-latency]: images/Buf_size.png
[ioctl-read]: images/ioctl_read.png
[ioctl-write]: images/ioctl_write.png
[transfer-read]: images/transfer_read.png
[transfer-write]: images/transfer_write.png